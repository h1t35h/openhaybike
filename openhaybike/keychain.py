"""Reads the MacOS keychain."""
from dataclasses import dataclass
from openhaybike.utils import bytes_to_int
import os

@dataclass
class Keychain:
    icloud_key_IV: bytes
    icloud_key_enc: bytes
    symmetric_key_IV: bytes
    symmetric_key_enc: bytes
    db_key_salt: bytes
    db_key_IV: bytes
    db_key_enc: bytes
    def __init__(self) -> None:
        pass

def read_keychain() -> Keychain:
    # https://github.com/libyal/dtformats/blob/main/documentation/MacOS%20keychain%20database%20file%20format.asciidoc
    result = Keychain()
    with open(
        "%s/Library/Keychains/login.keychain-db" % os.path.expanduser("~"), "rb"
    ) as db:
        kc = db.read()

        def get_table_offsets(tbl_array_offset):
            ntables = bytes_to_int(kc[tbl_array_offset + 4 : tbl_array_offset + 8])
            tbl_offsets_b = kc[
                tbl_array_offset + 8 : tbl_array_offset + 8 + (ntables * 4)
            ]
            return [
                bytes_to_int(tbl_offsets_b[i : i + 4]) + tbl_array_offset
                for i in range(0, len(tbl_offsets_b), 4)
            ]

        def get_record_offsets(tbl_start):
            nrecords = bytes_to_int(kc[tbl_start + 24 : tbl_start + 28])
            rec_offsets_b = kc[tbl_start + 28 : tbl_start + 28 + (nrecords * 4)]
            rec_offsets = [
                bytes_to_int(rec_offsets_b[i : i + 4]) + tbl_start
                for i in range(0, len(rec_offsets_b), 4)
            ]
            return [
                ro
                for ro in rec_offsets
                if ro != tbl_start and bytes_to_int(kc[ro : ro + 4])
            ]  # remove 0 offset records and empty records

        def match_record_attribute(rec_start, rec_nattr, rec_attr, attr_match):
            attr_offsets_b = kc[rec_start + 24 : rec_start + 24 + (rec_nattr * 4)]
            attr_offsets = [
                bytes_to_int(attr_offsets_b[i : i + 4]) + rec_start - 1
                for i in range(0, len(attr_offsets_b), 4)
            ]
            if attr_offsets[0] and attr_offsets[0] < rec_start + bytes_to_int(
                kc[rec_start : rec_start + 4]
            ):  # non-zero offset, and no weird big values
                if (
                    kc[
                        attr_offsets[rec_attr]
                        + 4 : attr_offsets[rec_attr]
                        + 4
                        + bytes_to_int(
                            kc[attr_offsets[rec_attr] : attr_offsets[rec_attr] + 4]
                        )
                    ]
                    == attr_match
                ):
                    return kc[
                        rec_start
                        + 24
                        + (rec_nattr * 4) : rec_start
                        + 24
                        + (rec_nattr * 4)
                        + bytes_to_int(kc[rec_start + 16 : rec_start + 20])
                    ]  # return record blob data (NOTE not sure about BLOB size!!!)
            return None

        if kc[:4] == b"kych":
            tbl_offsets = get_table_offsets(bytes_to_int(kc[12:16]))
            symmetric_key_idx = None
            for tbl_start in tbl_offsets[
                ::-1
            ]:  # walk backwards so we get the generic password blob before the symmetric key, we need that to select which key to take
                if (
                    kc[tbl_start + 4 : tbl_start + 8] == b"\x00\x00\x00\x11"
                ):  # Symmetric key
                    rec_offsets = get_record_offsets(tbl_start)
                    for rec_start in rec_offsets:
                        symmetric_key_blob = match_record_attribute(
                            rec_start, 27, 1, symmetric_key_idx
                        )  # might be wrong about amount of attributes
                        if symmetric_key_blob:
                            start_crypto_blob = bytes_to_int(symmetric_key_blob[8:12])
                            total_length = bytes_to_int(symmetric_key_blob[12:16])
                            result.symmetric_key_IV = symmetric_key_blob[16:24]
                            result.symmetric_key_enc = symmetric_key_blob[
                                24
                                + (start_crypto_blob - 0x18) : 24
                                + (total_length - 0x18)
                            ]
                            break
                elif (
                    kc[tbl_start + 4 : tbl_start + 8] == b"\x80\x00\x00\x00"
                ):  # Generic passwords
                    rec_offsets = get_record_offsets(tbl_start)
                    for rec_start in rec_offsets:
                        icloud_key_blob = match_record_attribute(
                            rec_start, 16, 14, b"iCloud"
                        )  # generic password record has 16 attributes
                        if icloud_key_blob:
                            symmetric_key_idx = icloud_key_blob[:20]
                            result.icloud_key_IV = icloud_key_blob[20:28]
                            result.icloud_key_enc = icloud_key_blob[28:]
                            break
                elif (
                    kc[tbl_start + 4 : tbl_start + 8] == b"\x80\x00\x80\x00"
                ):  # Metadata, containing master key and db key
                    rec_start = get_record_offsets(tbl_start)[0]
                    db_key_blob = kc[
                        rec_start
                        + 24 : rec_start
                        + 24
                        + bytes_to_int(kc[rec_start + 16 : rec_start + 20])
                    ]  # 2nd record is the one we want
                    result.db_key_salt = db_key_blob[44:64]
                    result.db_key_IV = db_key_blob[64:72]
                    result.db_key_enc = db_key_blob[120:168]
    return result
