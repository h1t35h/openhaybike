"""Types used in openhaybike."""
from dataclasses import dataclass
from datetime import datetime
from typing import Dict, Any

@dataclass(frozen=True, eq=True)
class LocationReport:
    latitude: float
    longitude: float
    confidence: int
    status: int
    timestamp: datetime

    def serialize(self) -> Dict[str, Any]:
        """Return serialization needed for cykel endpoint."""
        return {
            "lat": self.latitude,
            "lng": self.longitude,
            "accuracy": self.confidence,
            "reported_at": int(self.timestamp.timestamp())
        }

@dataclass
class BikeTracker:
    name: str
    key_id: str
    advertisement_key: str
    private_key: str
