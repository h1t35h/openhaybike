"""Utility functions."""

import hashlib
from cryptography.hazmat.primitives.ciphers import Cipher, CipherAlgorithm
from cryptography.hazmat.primitives.ciphers.modes import Mode
from cryptography.hazmat.primitives.padding import PKCS7
from cryptography.hazmat.backends import default_backend
import codecs


def bytes_to_int(b: str) -> int:
    return int(codecs.encode(b, 'hex'), 16)


def sha256(data: bytes) -> bytes:
    digest = hashlib.new("sha256")
    digest.update(data)
    return digest.digest()


def decrypt(enc_data: bytes, algorithm: CipherAlgorithm, mode: Mode) -> bytes:
    decryptor = Cipher(algorithm, mode, default_backend()).decryptor()
    return decryptor.update(enc_data) + decryptor.finalize()


def unpad(padded_bytes: bytes, blocksize: int) -> bytes:
    unpadder = PKCS7(blocksize).unpadder()
    return unpadder.update(padded_bytes) + unpadder.finalize()
